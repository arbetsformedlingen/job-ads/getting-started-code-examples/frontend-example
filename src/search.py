import json
import logging
import requests

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
logger = logging.getLogger(__name__)

url = 'https://links.api.jobtechdev.se'
endpoint = f"{url}/joblinks"


def get_with_params(params):
    headers = {'accept': 'application/json'}
    params['sort'] = 'relevance'  # 'pubdate-desc' for newest job ads first
    logger.info(f"SEARCH QUERY: {params}")
    r = requests.get(endpoint, params=params, headers=headers)
    r.raise_for_status()
    return json.loads(r.content.decode('utf8'))


def search(query, offset, limit):
    params = {'q': query} if query else {}
    params.update({'offset': offset, 'limit': limit})
    return get_with_params(params)
