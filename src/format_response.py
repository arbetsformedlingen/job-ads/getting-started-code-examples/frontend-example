from fastapi.templating import Jinja2Templates

templates = Jinja2Templates(directory='src/templates/')


def search_response(request, query, offset, limit, result):
    return templates.TemplateResponse('search.html',
                                      context={'request': request,
                                               'query': query,
                                               'offset': offset,
                                               'limit': limit,
                                               'result': result})


def get_response(request):
    return templates.TemplateResponse('search.html',
                                      context={'request': request,
                                               'query': ''})


def extract_from_ads(result):
    number_of_hits = result['total']['value']
    hits = result['hits']
    formatted_hits = []
    for hit in hits:
        ad = {}
        ad['headline'] = hit.get('headline', '')
        ad['first_url'] = hit.get('source_links', [])[0].get('url')
        ad['short_desc'] = hit.get('brief', '')
        ad['published'] = f"Published: {format_date(hit.get('publication_date'))}"
        formatted_hits.append(ad)
    return {'hits': formatted_hits,
            'number_of_hits': number_of_hits}


def format_date(in_date):
    return in_date.split('T')[0]
